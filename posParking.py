import cv2
import pickle
import imutils


width, height = 35, 81
try: 
    with open('carParkPos', 'rb') as f: #crea un archivo el cual contara con las areas de interes generadas, esto lo hace a partir de convertir objetos a bytes.
        posList = pickle.load(f)
except:
    posList = []        

def mouseClick(events, x, y, flags, params): #Funcion la cual crea area de interes teniendo un tamaño x,y definidos
    if events == cv2.EVENT_LBUTTONDOWN:#Al hacer click izquierdo agrega un area de interes a la lista
        posList.append((x, y))
    if events == cv2.EVENT_RBUTTONDOWN:#AL hacer clicl derecho elimina areas de interes de la lista y todos los que se encuentren en cierto area.
        for i,pos in enumerate(posList):
            x1, y1 = pos
            if x1<x<x1+width and y1<y<y1+height:
                posList.pop(i)
    with open('carParkPos','wb') as f:#transforma estas areas de interes a bytes, creado un nuevo archivo
        pickle.dump(posList, f)
        
while True:
    img = cv2.imread('estacionamiento.png')
    img =imutils.resize(img, width=600)

    for pos in posList:#crea un rectangulo de un tamaño establecido y los agrega a una lista
        cv2.rectangle(img, pos,(pos[0]+width,pos[1]+height),(255,0,255),2)
    
    cv2.imshow('Image',img)
    cv2.setMouseCallback('Image',mouseClick)
    
    k= cv2.waitKey(5) & 0xFF
    if k == 27:
        break
