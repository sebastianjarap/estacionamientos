import cv2
import numpy as np
import cvzone
import imutils
from PIL import Image
import pytesseract

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
paternText = []

image = cv2.imread('Auto.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.blur(gray,(3,3))
canny = cv2.Canny(gray,200,200)
canny=cv2.dilate(canny,None, iterations=1)

cnts,_ = cv2.findContours(canny, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
#cv2.drawContours(image,cnts,-1,(255,0,255))

for cnt in cnts:
    area = cv2.contourArea(cnt)
    x,y,w,h = cv2.boundingRect(cnt)
    epsilon = 0.09*cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,epsilon,True)
    if area > 4000 and area < 5500:
        #print('area', area)
        cv2.drawContours(image,[cnt],0,(9,255,0),2)
        aspect_ratio = float(w)/h
        print(aspect_ratio)
        if aspect_ratio >2.4:
            paternText=gray[y:y+h,x:x+w]
            text=pytesseract.image_to_string(paternText, config='--psm 6') #9 6

            print('text-',text)
            cv2.imshow('placa',paternText)
            cv2.moveWindow('placa',780,10)
            cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),3)
            cv2.putText(image,text,(x-20,y-10),1,2.2,(9,255,0),3)
            #cv2.drawContours(image,[cnt],0,(9,255,0),2)

cv2.imshow('Image',image)
#cv2.imshow('Canny',canny)

cv2.moveWindow('Image',100,10)
cv2.waitKey(0)