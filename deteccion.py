import pickle
import cv2
import numpy as np
import cvzone
import imutils

cap = cv2.VideoCapture('videoEstacionamiento.mp4')
kernel=np.ones((3,3),np.uint8)


with open('carParkPos', 'rb') as f: #Archivo creado por pickle el cual genera puntos de interes en el video, a partir de una imagen de este
    posList = pickle.load(f)

width, height = 35, 81 #Este espacio fue creado a partir de ir provando diferentes valores hasta cuadrar un espacio en el estacionamiento

def ParkingSpace(imgPro): #La funcion trabaja los espacios generados en posParking, la cual genero el archivo 'carParkPos' para generar puntos de interes objetivos
    spaceFree= 0
    for pos in posList: # Recorre la lista donde se encuentran los puntos de interes
        x,y=pos
        imgCrop=imgPro[y:y+height,x:x+width]
        #cv2.imshow(str(x*y),imgCrop)
        count = cv2.countNonZero(imgCrop) #Cuenta los pixeles blancos de la imagen, para saber si hay un auto o no
        cvzone.putTextRect(img,str(count),(x,y+height-10), scale=0.8,thickness=1,offset=0, colorR=(0,0,255))
        
        if count<600: #Gracias a countNonZero podemos saber que si hay menos de 600 hay un auto en el espacio del estacionamiento, en caso contrario este esta ocupado
            color = (0,255,0)
            thickness = 2
            spaceFree+=1
        else:
            color=(0,0,255)
            thickness = 2
        cv2.rectangle(img, pos,(pos[0]+width,pos[1]+height),color,thickness)
        
    cvzone.putTextRect(img, f'Libres: {spaceFree}/{len(posList)}',(500, 200), scale=1,thickness=1,
                    offset=0, colorR=(0,200,0))
while True:
    success, img = cap.read()
    img =imutils.resize(img, width=600) #Ajustamos la imagen para estandarizar un tamaño
    
    if cap.get(cv2.CAP_PROP_POS_FRAMES) == cap.get(cv2.CAP_PROP_FRAME_COUNT): # Se creara un loop en el video, para que no se cierre
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        
    #Se utilizan funciones para poder visualizar de mejor manera los frames de la imagen, para luego contar los pixeles 
    imgGray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    imgBLUR = cv2.GaussianBlur(imgGray,(3,3),1)
    imgThreshold=cv2.adaptiveThreshold(imgBLUR,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                    cv2.THRESH_BINARY_INV,25,16)
    imgMedian = cv2.medianBlur(imgThreshold,5)
    imgDilate=cv2.dilate(imgMedian, kernel, iterations=1)
    
    ParkingSpace(imgDilate)

    cv2.imshow('Image',img)
    #cv2.imshow('ImageBlur', imgBLUR)
    #cv2.imshow('ImageThreshold', imgThreshold)
    #cv2.imshow('ImageMedian', imgMedian)
    k= cv2.waitKey(100) & 0xFF
    if k == 27:
        break

cap.release()
